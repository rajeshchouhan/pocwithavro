#!/bin/bash -e


function getRepoSHA1List {

	pushd "$1" >/dev/null
	git rev-list --all
	popd >/dev/null
}

function exportCommits {
	local commitSHA1
	local IFS=$'\n'
	for commitSHA1 in $(getRepoSHA1List "$1"); do
		# build exporting directory for commit and create
		local exportDir="${2%%/}/$commitSHA1"
		#echo "Exporting $commitSHA1 -> $exportDir"

		mkdir --parents "$exportDir"

		# create archive from commit then unpack to export directory
		git \
			--git-dir "$1/.git" \
			archive \
			--format tar \
			"$commitSHA1" | \
				tar \
					--directory "$exportDir" \
					--extract
	break;				
	done
    echo $commitSHA1;
}


# execute
SHA=$(exportCommits "$1" "$2")

files=$(git diff-tree --no-commit-id --name-only -r $SHA)

filesArr=($files)

echo $files

# mkdir finalChangedFIles